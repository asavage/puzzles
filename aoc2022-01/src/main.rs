use std::io::stdin;

struct Leaderboard {
    first: u32,
    second: u32,
    third: u32,
}

impl Leaderboard {
    fn track(&mut self, value: u32) {
        if self.first < value {
            self.third = self.second;
            self.second = self.first;
            self.first = value;
        } else if self.second < value {
            self.third = self.second;
            self.second = value;
        } else if self.third < value {
            self.third = value;
        }
    }
}

fn main() {
    let mut leaderboard = Leaderboard {
        first: 0,
        second: 0,
        third: 0,
    };

    let mut value = 0;

    for line in stdin().lines().flatten() {
        match line.as_str() {
            "" => {
                leaderboard.track(value);
                value = 0;
            }
            _ => {
                value += line.parse::<u32>().unwrap();
            }
        }
    }

    leaderboard.track(value); // in case there's no trailing empty line

    println!();
    println!("  Largest: {}", leaderboard.first);
    println!(
        "Top three: {}",
        leaderboard.first + leaderboard.second + leaderboard.third
    );
}
